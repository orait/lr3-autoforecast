from sklearn import linear_model


def gen_lasso(model_parameter):
    """
    文档链接：https://kxifss6gg9.feishu.cn/wiki/wikcnp5ziQBUNScauelqcEIqenc#
    :param model_parameter:
    :return:
    """
    model = linear_model.Lasso(**model_parameter)
    return model