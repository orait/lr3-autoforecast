"""
微服务：日志生成-V1.00
"""
import os
import logging
import sys
import platform


class GenLogger(object):

    def __init__(self, logger_file_name):
        self.logger_file_name = logger_file_name
        self.logger = self.logger_generator()

    def logger_generator(self):

        sys_type = platform.system()
        current_dir = os.path.dirname(os.path.realpath(__file__))
        if sys_type == "Windows":
            # current_path = user_input[0:user_input.rfind('\\') + 1]
            logfile = current_dir + '\\' + self.logger_file_name
        else:
            logfile = current_dir + '/' + self.logger_file_name
        print(logfile)

        logger = logging.getLogger()
        fh = logging.FileHandler(filename=logfile, mode='a', encoding='utf-8')
        ch = logging.StreamHandler()
        formatter = logging.Formatter('%(levelname)s:-%(asctime)s-%(thread)d-'
                                      '%(filename)s[%(funcName)s][line:%(lineno)d]%(message)s')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        logger.addHandler(ch)
        logger.addHandler(fh)

        # 根据用户输入设置日志级别
        try:
            logLevel = sys.argv[1].lower()
            if logLevel == 'critical':
                logger.setLevel(logging.CRITICAL)
            elif logLevel == 'error':
                logger.setLevel(logging.ERROR)
            elif logLevel == 'warning':
                logger.setLevel(logging.WARNING)
            elif logLevel == 'info':
                logger.setLevel(logging.INFO)
            elif logLevel == 'debug':
                logger.setLevel(logging.DEBUG)
            else:
                logger.debug('wrong input log level use default level')
                logger.setLevel(logging.WARNING)
        except IndexError:
            logger.debug('User not input log level use default level')
            logger.setLevel(logging.INFO)
        return logger

    def get_logger(self):
        return self.logger

