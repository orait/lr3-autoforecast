from scipy.stats import pearsonr
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_percentage_error
import datetime
# test
y_predict = [1.5, 2.5, 3.5, 10.5]
y_true = [1, 2, 3, 10]

pccs, p_value = pearsonr(y_predict, y_true)
mapes = 1 - mean_absolute_percentage_error(y_true, y_predict)
r2 = r2_score(y_true, y_predict)

print('pearson score is: ', pccs)
print('1-mape score is: ', mapes)
print('r2 score is: ', r2)

test_list = ['torch']

if 'autots' in test_list:
    from autots import AutoTS
    from autots.datasets import load_hourly
    print(datetime.datetime.now())
    df_wide = load_hourly(long=False)
    print(df_wide)
    print(len(df_wide))

    # here we care most about traffic volume, all other series assumed to be weight of 1
    weights_hourly = {'traffic_volume': 20}

    model_list = [
        'LastValueNaive',
        'GLS',
        'ETS',
        'AverageValueNaive',
    ]

    model = AutoTS(
        forecast_length=49,
        frequency='infer',
        prediction_interval=0.95,
        ensemble='simple',
        max_generations=5,
        num_validations=2,
        validation_method='seasonal 168',
        model_list=model_list,
        transformer_list='all',
        models_to_validate=0.2,
        drop_most_recent=1,
        n_jobs='auto',
    )
    result_file = ''
    model = model.fit(
        df_wide,
        weights=weights_hourly,
        result_file= ''
    )

    prediction = model.predict()
    forecasts_df = prediction.forecast

    print(datetime.datetime.now())
    print('xxxxxxxxxx')
    print(prediction)
    print(forecasts_df)

if 'torch' in test_list:
    import torch
    x = torch.rand(5, 3)
    print(x)

