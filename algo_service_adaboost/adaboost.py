from sklearn.ensemble import AdaBoostClassifier


def gen_adaboost(model_parameter):
    """
    文档链接：https://kxifss6gg9.feishu.cn/wiki/wikcna6jrSBlXaM366o1XpYdRih#
    :param model_parameter:
    :return:
    """
    model = AdaBoostClassifier(**model_parameter)
    return model
