import statsmodels.api as sm
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_percentage_error
from scipy.stats import pearsonr


def gen_sarimax(model_parameter):
    """
    文档链接：https://kxifss6gg9.feishu.cn/wiki/wikcnd34Kr07oMjezy5BuWnjtEd#
    :param model_parameter:
    :return:
    """
    model = sm.tsa.statespace.SARIMAX(**model_parameter)
    return model


def sarimax_score_mape(y_predict, y_true):
    # NOTE, the input position is switched.
    mape_score = 1 - mean_absolute_percentage_error(y_true, y_predict)
    return mape_score


def sarimax_score_pearson(y_predict, y_true):
    # need to edit inputs to calculate
    pccs = pearsonr(y_predict, y_true)
    return pccs


def sarimax_score(y_predict, y_true):
    """
    R2 is also used in svr, lasso, ridge regression, xgboost, etc
    :param y_predict:
    :param y_true:
    :return:
    """
    # ref https://stats.stackexchange.com/questions/12900/when-is-r-squared-negative
    # NOTE, the input position is switched.
    r2score = r2_score(y_true, y_predict)
    return r2score




