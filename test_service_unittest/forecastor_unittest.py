import unittest
from sklearn.datasets import make_regression
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_friedman2
from algo_service_forecastor.forecastor import Forecastor
from sklearn.gaussian_process.kernels import DotProduct, WhiteKernel
from pandas_datareader.data import DataReader
import pandas as pd
import numpy as np


def gen_regression_data():
    X, y = make_regression(n_samples=200, random_state=1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
    return X_train, X_test, y_train, y_test


def gen_classification_data():
    X, y = make_classification(n_samples=200, random_state=1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
    return X_train, X_test, y_train, y_test


def gen_auto_regression_data():
    cpi = DataReader('CPIAUCNS', 'fred', start='1971-01', end='2018-12')
    cpi.index = pd.DatetimeIndex(cpi.index, freq='MS')
    # Define the inflation series that we'll use in analysis
    inf = np.log(cpi).resample('QS').mean().diff()[1:] * 400
    tr_start, tr_end = '2000-01-01', '2010-10-01'
    te_start, te_end = '2011-01-01', '2018-10-01'
    inf_train = inf[tr_start: tr_end].dropna()
    # print(inf_train)
    inf_test = inf[te_start: te_end].dropna()
    return inf_train, inf_test


def get_algorithm_dict():
    auto_regression_list = ['sarimax', 'x13_arima']
    linear_regression_list = ['ridge_regression', 'lasso', 'svr', 'xgboost']
    classifier_list = ['gbdt', 'adaboost']

    algorithm_dict = {'auto_regression_list': auto_regression_list,
                      'linear_regression_list': linear_regression_list,
                      'classifier_list': classifier_list}
    return algorithm_dict


class TestForecastor(unittest.TestCase):
    def tearDown(self):
        algorithm_dict = get_algorithm_dict()
        fc = Forecastor(self.model_name, self.model_parameter)

        if self.model_name in algorithm_dict['linear_regression_list']:
            X_train, X_test, y_train, y_test = gen_regression_data()
            fc.fit(X_train, y_train)
            print(self.model_name + " prediction is: ", fc.predict(X_test[:2]))
            print(self.model_name + " score is: ", fc.score(X_test, y_test))

        elif self.model_name in algorithm_dict['classifier_list']:
            X_train, X_test, y_train, y_test = gen_classification_data()
            fc.fit(X_train, y_train)
            print(self.model_name + " prediction is: ", fc.predict(X_test[:2]))
            print(self.model_name + " score is: ", fc.score(X_test, y_test))

        elif self.model_name in algorithm_dict['auto_regression_list']:
            y_train_auto, y_test_auto = gen_auto_regression_data()
            fc.fit(disp=False)
            steps = 12
            print(self.model_name + " prediction is : ", fc.predict(steps=steps))
            print(self.model_name + " score is: ", fc.score(y_test_auto[:steps]))

        else:
            print('not supported')

    def test_svr(self):
        self.model_name = 'svr'
        self.model_parameter = {'C': 1.0, 'cache_size': 200, 'coef0': 0.0, 'degree': 3, 'epsilon': 0.1, 'gamma': 'auto',
                                'kernel': 'rbf', 'max_iter': -1, 'shrinking': True, 'tol': 0.001, 'verbose': False}  #

    def test_lasso(self):
        self.model_name = 'lasso'
        self.model_parameter = {'alpha': 1.0}

    def test_ridge_regression(self):
        self.model_name = 'ridge_regression'
        self.model_parameter = {'alpha': 1.0}

    def test_adaboost(self):
        self.model_name = 'adaboost'
        self.model_parameter = {'n_estimators': 100, 'learning_rate': 0.5}

    def test_gbdt(self):
        self.model_name = 'gbdt'
        self.model_parameter = {'n_estimators': 100, 'learning_rate': 0.5}

    def test_xgboost(self):
        self.model_name = 'xgboost'
        self.model_parameter = {'objective': 'reg:linear'}
        # self.model_parameter = {'objective': 'reg:squarederror'}

    def test_sarimax(self):
        self.model_name = 'sarimax'
        y_train_auto, y_test_auto = gen_auto_regression_data()
        self.model_parameter = {'endog': y_train_auto, 'order': (1, 0, 1)}


if __name__ == '__main__':
    unittest.main()
