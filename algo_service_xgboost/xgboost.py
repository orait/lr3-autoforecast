from xgboost import XGBClassifier
import xgboost as xgb


def gen_xgboost_calssfier(model_parameter):
    """
    文档链接：
    :param model_parameter:
    :return:
    """
    model = XGBClassifier(**model_parameter)
    return model


def gen_xgboost(model_parameter):
    """
    文档链接：
    :param model_parameter:
    :return:
    """
    model = xgb.XGBRegressor(**model_parameter)
    return model


'''
try:
    from xgboost import XGBClassifier
except Exception as error:
    print('fail to import xgboost, reason is %s ' % error)
else:
    def gen_xgboost(model_parameter):
        """
        文档链接：
        :param model_parameter:
        :return:
        """
        model = XGBClassifier(**model_parameter)

        return model
'''


