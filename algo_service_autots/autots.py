from autots import AutoTS


def gen_autots(model_parameter):
    """
    ref：https://winedarksea.github.io/AutoTS/build/html/source/tutorial.html#

    Model Lists¶
    By default, most available models are tried. For a more limited subset of models, a custom list can be passed in,
    or more simply, a string, one of 'probabilistic', 'multivariate', 'fast', 'superfast', or 'all'.
    A table of all available models is available further below.
    On large multivariate series, TSFreshRegressor, DynamicFactor and VARMAX can be impractically slow.

    :param model_parameter:
    :return:

    example:
    from autots import AutoTS
    from autots.datasets import load_hourly

    df_wide = load_hourly(long=False)

    # here we care most about traffic volume, all other series assumed to be weight of 1
    weights_hourly = {'traffic_volume': 20}

    model_list = [
        'LastValueNaive',
        'GLS',
        'ETS',
        'AverageValueNaive',
    ]

    model = AutoTS(
        forecast_length=49,
        frequency='infer',
        prediction_interval=0.95,
        ensemble='simple',
        max_generations=5,
        num_validations=2,
        validation_method='seasonal 168',
        model_list=model_list,
        transformer_list='all',
        models_to_validate=0.2,
        drop_most_recent=1,
        n_jobs='auto',
    )

    model = model.fit(
        df_wide,
        weights=weights_hourly,
    )

    prediction = model.predict()
    forecasts_df = prediction.forecast
    # model.best_model.to_string()
    """
    model = AutoTS(**model_parameter)
    return model
