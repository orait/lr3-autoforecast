from sklearn import svm


def gen_svr(model_parameter):
    """
    根据指定的model_parameter,生成并返回sklearn中的svr模型。

    sklearn中包含多种机器学习模型，该函数直接调用sklearn中svr模型生成方法svm.SVR()，注意，不同模型的适用传入参数
    model_parameter是不一样的，需要查询sklearn包。

    Args:
        model_parameter (dict): svr回归模型中控制参数。范例：(C=1.0, cache_size=200, coef0=0.0, degree=3, epsilon=0.1, gamma='auto_deprecated',
 kernel='rbf', max_iter=-1, shrinking=True, tol=0.001, verbose=False)。

    Returns:
        model: sklearn中的svm.SVR()模型
    """
    model = svm.SVR(**model_parameter)
    return model
