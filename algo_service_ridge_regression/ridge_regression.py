from sklearn import linear_model


def gen_ridge_regression(model_parameter):
    """
    文档链接：https://kxifss6gg9.feishu.cn/wiki/wikcnU21TXp89TXV0EtwPBF1aff
    根据指定的model_parameter,生成并返回sklearn中的岭回归模型（ridge regression）。
    sklearn中包含多种机器学习模型，该函数直接调用sklearn中岭回归模型生成方法linear_model.Ridge()，注意，不同模型的适用传入参数
    model_parameter是不一样的，需要查询sklearn包。

    Args:
        model_parameter (dict): 岭回归模型中控制参数。

    Returns:
        model: sklearn中的linear_model.Ridge模型
    """
    model = linear_model.Ridge(**model_parameter)
    return model
