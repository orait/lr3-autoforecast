# from sklearn.datasets import make_hastie_10_2
from sklearn.ensemble import GradientBoostingClassifier


def gen_gbdt(model_parameter):
    """
    文档链接：https://kxifss6gg9.feishu.cn/wiki/wikcnVTUSKTcawbY8g4TH0Xtgge#hmZ5pC
    :param model_parameter:
    :return:
    """
    model = GradientBoostingClassifier(**model_parameter)
    return model

