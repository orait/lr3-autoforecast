"""
LR3-XX-算法调用-V1.00
"""
import os

# import logger
from data_service_logger import logger

# import algorithms
from algo_service_svr import svr
from algo_service_LASSO import LASSO
from algo_service_ridge_regression import ridge_regression
from algo_service_adaboost import adaboost
from algo_service_xgboost import xgboost
from algo_service_GBDT import gbdt
from algo_service_sarimax import sarimax


def gen_model_function_dict():
    # todo modify as necessary
    model_function_dict = \
        {'ridge_regression': ridge_regression.gen_ridge_regression,
         'lasso': LASSO.gen_lasso,
         'svr': svr.gen_svr,
         'adaboost': adaboost.gen_adaboost,
         'gbdt': gbdt.gen_gbdt,
         'xgboost': xgboost.gen_xgboost,
         'sarimax': sarimax.gen_sarimax,

         }
    return model_function_dict


class Forecastor(object):
    """
    预测子类，根据输入的模型名model_name、模型参数model_parameter，生成对应的预测模型，提供模型训练fit()、评分score()、预测forecast()、
    访问训练参数getW()、getIntercept()等功能。

    sklearn中包含多种机器学习模型（既包含回归，也包含分类、无监督聚类模型），Forecastor类将sklearn中回归类模型封装成预测对象类，并将sklearn中的模型函数fit()、
    score()、predict()等封装成预测对象的函数。

    Args:
        model_name(str):模型名。
        model_parameter (dict): 模型控制参数，例如linear_model.LinearRegression()中的{'copy_X':True, 'fit_intercept':True,
        'n_jobs':1, 'normalize':False}参数。

    funcs:
        gen_model(self):模型生成函数，根据model_name和model_parameter，调用sklearn包，生成model对象。
        fit(self,X,Y): 训练函数，将X和Y输入forecastor中训练，得到训练好的预测器fc。
        score(self,X,Y):评分函数，用X输入model中，评估其输出Y'与Y的差距，计算得到预测器的预测分数。
        predict(self,X):预测函数，将X输入model中，预测得到Y。
    """
    def __init__(self, model_name, model_parameter):

        log_file_name = 'log_forecastor.txt'
        self.__loggerHandler = logger.GenLogger(log_file_name).get_logger()
        self.model_name = model_name
        self.model_parameter = model_parameter
        self.model_function_dict = gen_model_function_dict()

        self.model = self.gen_model()

        self.score_value = 0
        self.auto_regression_list = ['sarimax']
        self.fit_result = None
        self.predict_result = None

    def gen_model(self):
        """
        模型生成函数，根据model_name和model_parameter，调用sklearn包，生成model对象。

        sklearn中包含多种机器学习模型，该函数内置model_name与模型构造函数gen_model_func之间的字典对应关系，根据传入的model_name找到对应的
        gen_model_func并调用它生成model，并将model返回给self.model。

        Args:
            self:对象自己。#

        Returns:
            model: sklearn中的模型对象，例如linear_model.LinearRegression()。
        """
        self.__loggerHandler.debug('entry')
        model2func = self.model_function_dict
        model = None
        # 此处lambda为false，如果字典查找不到，那返回的是false
        gen_model_func = model2func.get(self.model_name, False)

        # 如果匹配到gen_model_func，则调用对应构造函数，生成model
        if gen_model_func:
            model = gen_model_func(self.model_parameter)
        else:
            self.__loggerHandler.warning('model name: %s not found.' % self.model_name)
        return model

    def fit(self, X=None, Y=None, *args, **kwargs):
        """训练函数，将X和Y输入forecastor中训练，得到训练好的预测器fc

        sklearn中机器学习模型大多已经包含fit(X,Y)函数，因此可直接调用self.model.fit(X,Y)。

        Args:
            self:对象自己。
            X:自变量，列为特征，行为样本记录。
            Y:因变量。

        Returns:
            直接修改self.model。
        """
        self.fit_result = self.model.fit(X, Y, *args, **kwargs)
        return self

    def predict(self, X=None, *args, **kwargs):
        """预测函数，将X输入model中，预测得到Y。

        sklearn中机器学习模型大多已经包含predict(X)函数，因此可直接调用self.model.predict(X)。注意，本函数名为forecast，非predict。

        Args:
            self:对象自己。
            X:自变量，列为特征，行为样本记录。

        Returns:
            Y:因变量。
        """
        if self.model_name in self.auto_regression_list:
            # Y = self.fit_result.get_prediction(**kwargs).predicted_mean
            self.predict_result = self.fit_result.forecast(*args, **kwargs)
        else:
            self.predict_result = self.model.predict(X)
        return self.predict_result

    def score(self, X=None, Y=None):
        """评分函数，用X输入model中，评估其输出Y'与Y的差距，计算得到预测器的预测分数。

        sklearn中机器学习模型大多已经包含score(X,Y)函数，因此可直接调用self.model.score(X,Y)。

        Args:
            self:对象自己。
            X:自变量，列为特征，行为样本记录。
            Y:因变量。

        Returns:
            直接修改self.score。
        """
        score_func_dict = {
            'sarimax': sarimax.sarimax_score,
        }

        if self.model_name in self.auto_regression_list:
            gen_score_func = score_func_dict.get(self.model_name, False)
            # below X is for y_true
            self.score_value = gen_score_func(self.predict_result, X)
        else:
            self.score_value = self.model.score(X, Y)
        return self.score_value


